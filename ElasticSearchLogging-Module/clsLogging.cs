﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ElasticSearchConfig_Module;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace ElasticSearchLogging_Module
{
    [Flags]
    public enum LogType
    {
        DEBUG = 1,
        INFO = 2,
        WARN = 4,
        ERROR = 8,
        FATAL = 16
    }
    public class clsLogging
    {
        private clsLocalConfig objLocalConfig;

        private clsDataWork_Index objDataWork_Index;
        private clsDataWork_Logging objDataWork_Logging;

        private clsOntologyItem objOItem_Log;

        private List<clsAppDocuments> objDocumentList;

        private clsAppDocuments objAppDocument_Current;

        private clsAppDBLevel objAppDBLevel;

        private string altType;

        private bool isActive;

        public clsLogging(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public clsLogging(Globals Globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        public clsOntologyItem LogDebug(string source, string message)
        {
            return LogSimple(LogType.DEBUG, source, message);
        }
        public clsOntologyItem LogError(string source, string message)
        {
            return LogSimple(LogType.ERROR, source, message);
        }
        public clsOntologyItem LogInfo(string source, string message)
        {
            return LogSimple(LogType.INFO, source, message);
        }
        public clsOntologyItem LogWarn(string source, string message)
        {
            return LogSimple(LogType.WARN, source, message);
        }
        public clsOntologyItem LogFatal(string source, string message)
        {
            return LogSimple(LogType.FATAL, source, message);
        }


        private clsOntologyItem LogSimple(LogType logType, string source, string message)
        {
            
            
            var result = objLocalConfig.Globals.LState_Success.Clone();
            if (isActive)
            {
                Init_Document(objLocalConfig.Globals.NewGUID);
                Add_DictEntry(Enum.GetName(typeof(LogType), LogType.DEBUG), (logType == LogType.DEBUG));
                Add_DictEntry(Enum.GetName(typeof(LogType), LogType.ERROR), (logType == LogType.ERROR));
                Add_DictEntry(Enum.GetName(typeof(LogType), LogType.FATAL), (logType == LogType.FATAL));
                Add_DictEntry(Enum.GetName(typeof(LogType), LogType.INFO), (logType == LogType.INFO));
                Add_DictEntry(Enum.GetName(typeof(LogType), LogType.WARN), (logType == LogType.WARN));
                Add_DictEntry("TimeStamp", DateTime.Now);
                Add_DictEntry("Source", source);
                Add_DictEntry("Message", message);
                Finish_Document();
            }
            else
            {
                result = objLocalConfig.Globals.LState_Nothing.Clone();
            }
            

            return result;
        }

        public clsOntologyItem Initialize_LoggingByModule(string GuidModule)
        {
            var logger = objDataWork_Logging.GetLoggerByGuidModule(GuidModule);
            if (logger  != null)
            {
                return Initialize_Logging(logger);
            }
            else
            {
                isActive = false;
                return objLocalConfig.Globals.LState_Error.Clone();
            }

        }

        public clsOntologyItem Initialize_Logging(clsOntologyItem OItem_Log, string altType = null)
        {
            this.altType = altType;
            objOItem_Log = OItem_Log;

            var objOItem_Result = objDataWork_Index.GetIndexData_OfRef(objOItem_Log, objLocalConfig.OItem_relationtype_logging, objLocalConfig.OItem_relationtype_logging, objLocalConfig.Globals.Direction_LeftRight);

            

            objOItem_Result = objDataWork_Logging.GetSubData_ItemCount(OItem_Log);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOItem_Result = objDataWork_Logging.GetSubData_IsActive(OItem_Log);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    if (objDataWork_Logging.IsActive)
                    {
                        objAppDBLevel = new clsAppDBLevel(objDataWork_Index.OItem_Server.Name, int.Parse(objDataWork_Index.OItem_Port.Name), objDataWork_Index.OItem_Index.Name, objDataWork_Logging.ItemCount, objLocalConfig.Globals.Session);
                        
                    }
                    isActive = objDataWork_Logging.IsActive;
                }
                else
                {
                    isActive = false;
                }
                
            }
            else
            {
                isActive = false;
            }

            objDocumentList = new List<clsAppDocuments>();

            return objOItem_Result;
        }

        public clsOntologyItem Add_DictEntry(string key, object item)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            try
            {
                objAppDocument_Current.Dict[key] = item;
            }
            catch (Exception ex)
            {
                objOItem_Result = objLocalConfig.Globals.LState_Error.Clone();
            }
            

            return objOItem_Result;
        }

        public void Init_Document(string Id)
        {
            objAppDocument_Current = new clsAppDocuments();
            objAppDocument_Current.Id = Id;
            objAppDocument_Current.Dict = new Dictionary<string, object>();
        }

        public clsOntologyItem Finish_Document()
        {
            objDocumentList.Add(objAppDocument_Current);

            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            if (isActive && objDocumentList.Count >= objDataWork_Logging.ItemCount)
            {
                objOItem_Result = objAppDBLevel.Save_Documents(objDocumentList, altType ?? objDataWork_Index.OItem_Type.Name, objDataWork_Index.OItem_Index.Name);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objDocumentList.Clear();
                }
            }

            return objOItem_Result;
        }

        public clsOntologyItem Flush_Documents()
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();


            if (isActive && objDocumentList != null && objDocumentList.Any())
            {
                objOItem_Result = objAppDBLevel.Save_Documents(objDocumentList, altType ?? objDataWork_Index.OItem_Type.Name, objDataWork_Index.OItem_Index.Name);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    objDocumentList.Clear();
                }
            
            }

            return objOItem_Result;
        }

        private void Initialize()
        {
            objDataWork_Index = new clsDataWork_Index(objLocalConfig.Globals);
            objDataWork_Logging = new clsDataWork_Logging(objLocalConfig);

            
        }
    }
}
