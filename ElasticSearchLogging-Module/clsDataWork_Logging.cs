﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OntologyClasses.BaseClasses;
using Ontology_Module;
using OntologyAppDBConnector;
using OntoMsg_Module;
using System.Runtime.InteropServices;

namespace ElasticSearchLogging_Module
{
    public class clsDataWork_Logging
    {
        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_ItemCount;
        private OntologyModDBConnector objDBLevel_Active;
        private OntologyModDBConnector objDBLevel_Logger;


        public int ItemCount { get; set; }
        public bool IsActive { get; set; }


        public clsOntologyItem GetLoggerByGuidModule(string GuidModule)
        {
            var search = new List<clsObjectRel> {new clsObjectRel {ID_Object = GuidModule,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_log__elasticsearch_.GUID}};

            var result = objDBLevel_Logger.GetDataObjectRel(search);

            if (result.GUID == objLocalConfig.Globals.LState_Success.GUID && objDBLevel_Logger.ObjectRels.Any())
            {
                return objDBLevel_Logger.ObjectRels.Select(log => new clsOntologyItem
                {
                    GUID = log.ID_Other,
                    Name = log.Name_Other,
                    GUID_Parent = log.ID_Parent_Other,
                    Type = objLocalConfig.Globals.Type_Object
                }).FirstOrDefault();

            }
            else
            {
                return null;
            }
        }

        public clsOntologyItem GetSubData_ItemCount(clsOntologyItem OItem_Log)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objOARel_ItemCount = new List<clsObjectAtt> { new clsObjectAtt {ID_AttributeType = objLocalConfig.OItem_attributetype_itemcountbeforelogging.GUID,
                ID_Object = OItem_Log.GUID } };

            objOItem_Result = objDBLevel_ItemCount.GetDataObjectAtt(objOARel_ItemCount, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_ItemCount.ObjAtts.Any())
                {
                    ItemCount = (int)objDBLevel_ItemCount.ObjAtts.First().Val_Int;
                }
                else
                {
                    objOItem_Result = objLocalConfig.Globals.LState_Error.Clone();
                }
            }

            return objOItem_Result;
        }

        public clsOntologyItem GetSubData_IsActive(clsOntologyItem OItem_Log)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var objOARel_ItemCount = new List<clsObjectAtt> { new clsObjectAtt {ID_AttributeType = objLocalConfig.OItem_attributetype_is_active.GUID,
                ID_Object = OItem_Log.GUID } };

            objOItem_Result = objDBLevel_Active.GetDataObjectAtt(objOARel_ItemCount, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_Active.ObjAtts.Any())
                {
                    IsActive = (bool)objDBLevel_Active.ObjAtts.First().Val_Bool;
                }
                else
                {
                    IsActive = false;
                }
            }

            return objOItem_Result;
        }

        public clsDataWork_Logging(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public clsDataWork_Logging(Globals Globals)
        {
            objLocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (objLocalConfig == null)
            {
                objLocalConfig = new clsLocalConfig(Globals);
                LocalConfigManager.AddLocalConfig(objLocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            objDBLevel_ItemCount = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Logger = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_Active = new OntologyModDBConnector(objLocalConfig.Globals);
        }
    }
}
